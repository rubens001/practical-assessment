package br.com.tecsinapse.practical.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ItemPedido {
	private String cnpjCliente;
	private String usuarioSolicitante;
	private String codigoItem;
	private int quantidade;
	private BigDecimal valorTotal;
	private BigDecimal valorUnitario;
	private boolean adicionado = false;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cnpjCliente == null) ? 0 : cnpjCliente.hashCode());
		result = prime
				* result
				+ ((usuarioSolicitante == null) ? 0 : usuarioSolicitante
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (cnpjCliente == null) {
			if (other.cnpjCliente != null)
				return false;
		} else if (!cnpjCliente.equals(other.cnpjCliente))
			return false;
		if (usuarioSolicitante == null) {
			if (other.usuarioSolicitante != null)
				return false;
		} else if (!usuarioSolicitante.equals(other.usuarioSolicitante))
			return false;
		return true;
	}

	public void adicionaPedido(ItemPedido novoItem) {
		this.quantidade = this.quantidade + novoItem.getQuantidade();
		this.valorTotal = this.valorTotal.add(novoItem.getValorTotal());
		this.valorUnitario = this.valorTotal.divide(new BigDecimal(this.quantidade)).setScale(2, RoundingMode.HALF_UP);
	}
	
	public boolean isAdicionado() {
		return adicionado;
	}

	public void setAdicionado(boolean adicionado) {
		this.adicionado = adicionado;
	}
	
	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getCnpjCliente() {
		return cnpjCliente;
	}

	public void setCnpjCliente(String cnpjCliente) {
		this.cnpjCliente = cnpjCliente;
	}

	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	public String getCodigoItem() {
		return codigoItem;
	}

	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

}

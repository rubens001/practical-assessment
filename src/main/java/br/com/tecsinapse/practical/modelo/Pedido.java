package br.com.tecsinapse.practical.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Pedido {
	private String cnpjCliente;
	private String usuarioSolicitante;
	private List<ItemPedido> itens = new ArrayList<>();
	private BigDecimal valorTotal;

	public BigDecimal getValorTotal() {
		BigDecimal soma = new BigDecimal(0D);
		for (ItemPedido item : itens) {
			soma = soma.add(item.getValorTotal()).setScale(2, RoundingMode.HALF_UP);
		}
		return soma;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getCnpjCliente() {
		return cnpjCliente;
	}

	public void setCnpjCliente(String cnpjCliente) {
		this.cnpjCliente = cnpjCliente;
	}

	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}

}
